<?php

/**
 * @file
 * Page callbacks for the administration of Pingdom check.
 */

function pingdom_check_admin_config() {
  $form = array();

  $form['pingdom_check_apikey'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#default_value' => variable_get('pingdom_check_apikey', ''),
    '#description' => t('The API key from Pingdom. You can find the key under http://my.pingdom.com -> sharing'),
    '#required' => TRUE,
  );

  $form['pingdom_check_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Pingdom user'),
    '#default_value' => variable_get('pingdom_check_user', ''),
    '#description' => t('The Pingdom user for accessing Pingdom.'),
    '#required' => TRUE,
  );

  $form['pingdom_check_pass'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#default_value' => variable_get('pingdom_check_pass', ''),
    '#description' => t('The password for the Pingdom user.'),
    '#required' => TRUE,
  );

  $form['pingdom_check_imagepath'] = array(
    '#type' => 'textfield',
    '#title' => t('Image path'),
    '#default_value' => variable_get('pingdom_check_imagepath', '/image'),
    '#description' => t('The image path for the icons, starting from the module path. Begin with /.'),
    '#required' => TRUE,
  );

  $form[] = array(
    '#markup'=> '<small>' . t('Icons from ') . l('doublejdesign', 'http://www.doublejdesign.co.uk/', array('attributes' => array('target' => '_blank'))) . '</small>',
  );

  return system_settings_form($form);
}