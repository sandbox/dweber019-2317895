*******************************************************************************

Pingdom check

Description:
-------------------------------------------------------------------------------

This module provides a block in which Pingdom data is displayed.
Eg. If you have Pingdom setup with some services and you like to
display the current state of the services to some users you just
have to install this module.


Installation & Use:
-------------------------------------------------------------------------------

1.  Enable module in module list located at administer > structure > modules.
2.  Go to admin/config/services/pingdom_check.
3.  Add required information.

Note:
-------------------------------------------------------------------------------
This module makes use of the Pingdom API and specially of the "check" API call.


Author:
-------------------------------------------------------------------------------
David Weber <david.weber.schenker@gmail.com>
https://www.drupal.org/user/2798287